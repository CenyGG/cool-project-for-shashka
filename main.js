const r = require('request-promise')
const fs = require('fs')
const {ExportToCsv} = require('export-to-csv')

const FILE_NAME = 'sashka_pidr.csv'


async function parseMarketData() {
    const options = {
        uri: 'https://api.coin360.com/coin/latest',
        json: true
    };

    const res = await r(options)
    const currencies = Object.keys(res)

    const toReturn = []

    for (const currency of currencies) {
        const {volume_24h, change_1h, change_24h, quotes: {USD: {price, volume, market_cap}}, timestamp} = res[currency]
        toReturn.push({
            currency,
            volume_24h,
            change_1h,
            change_24h,
            USD_price: price,
            USD_volume: volume,
            USD_market_cap: market_cap
        })
    }

    exportToCsv(toReturn)
}

function exportToCsv(obj) {
    const options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true,
        showTitle: true,
        title: 'Huy',
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true
    };

    const csvExporter = new ExportToCsv(options);

    const res = csvExporter.generateCsv(obj, true);
    fs.writeFile(FILE_NAME, res, function (err) {
        if (err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });

}

(async () => {
    await parseMarketData()
})();